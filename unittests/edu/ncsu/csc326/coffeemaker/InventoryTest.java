package edu.ncsu.csc326.coffeemaker;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.ncsu.csc326.coffeemaker.exceptions.InventoryException;
import edu.ncsu.csc326.coffeemaker.exceptions.RecipeException;

public class InventoryTest {

	@Test
	public void testAddInventory() {
		CoffeeMaker cmaker = new CoffeeMaker();

		try {
			cmaker.addInventory("10", "5", "4", "3");
		}catch (InventoryException e) {
			fail("Shouldn't throw exception");
		}
		assertTrue(verifyInventory(cmaker.checkInventory(), 25, 20, 19, 18));

		try {
			cmaker.addInventory("0", "0", "0", "0");
		}catch (InventoryException e) {
			fail("Shouldn't throw exception");
		}
		assertTrue(verifyInventory(cmaker.checkInventory(), 25, 20, 19, 18));

		try {
			cmaker.addInventory("2", "2", "2", "2");
		}catch (InventoryException e) {
			fail("Shouldn't throw exception");
		}
		assertTrue(verifyInventory(cmaker.checkInventory(), 27, 22, 21, 20));

		boolean hitException = false;
		try {
			cmaker.addInventory("1", "-1", "1", "1");
		}catch (InventoryException e) {
			hitException = true;
		}
		assertTrue(hitException);
		
		
	}
	
	@Test
	public void testMakeCoffee() {
		CoffeeMaker cmaker = new CoffeeMaker();
		
		Recipe r = new Recipe();
		try {
			r.setAmtChocolate("1");
			r.setAmtCoffee("2");
			r.setAmtMilk("3");
			r.setAmtSugar("3");
			r.setPrice("5");
		} catch (RecipeException e) {
			fail ("Shouldn't throw exception");
		}
		
		if (cmaker.addRecipe(r)) {
			/* More than enough money */
			int change = cmaker.makeCoffee(0, 10);
			assertEquals(change, 5);
			assertTrue(verifyInventory(cmaker.checkInventory(), 13, 12, 12, 14));
			
			/* Exactly enough money */
			change = cmaker.makeCoffee(0, 5);
			assertEquals(change, 0);
			assertTrue(verifyInventory(cmaker.checkInventory(), 11, 9, 9, 13));
			
			/* Not enough money */
			change = cmaker.makeCoffee(0, 2);
			assertEquals(change, 2);
			assertTrue(verifyInventory(cmaker.checkInventory(), 11, 9, 9, 13));
		}else {
			fail("Couldn't add recipe");
		}
	}
	
	@Test
	public void testCheckInventory() {
		CoffeeMaker cmaker = new CoffeeMaker();
		
		/* Check initial values for inventory */
		assertTrue(verifyInventory(cmaker.checkInventory(), 15, 15, 15, 15));
		
	}

	public boolean verifyInventory(String actual, int expectedCoffee, int expectedMilk, int expectedSugar, int expectedChocolate) {
		String expectedResult = "Coffee: " + Integer.toString(expectedCoffee) + 
			"\nMilk: " +  Integer.toString(expectedMilk) + 
			"\nSugar: " + Integer.toString(expectedSugar) + 
			"\nChocolate: " + Integer.toString(expectedChocolate) + "\n";
		if (actual.equals(expectedResult)) {
			return true;
		}
		return false;
	}
}
