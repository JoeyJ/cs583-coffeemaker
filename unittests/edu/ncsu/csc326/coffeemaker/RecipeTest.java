package edu.ncsu.csc326.coffeemaker;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.ncsu.csc326.coffeemaker.exceptions.RecipeException;

public class RecipeTest {

	@Test
	public void testAddRecipe() {
		CoffeeMaker cmaker = new CoffeeMaker();

		Recipe r = new Recipe();
		try {
			r.setAmtChocolate("1");
			r.setAmtCoffee("2");
			r.setAmtMilk("3");
			r.setAmtSugar("3");
			r.setPrice("5");
		} catch (RecipeException e) {
			fail ("Shouldn't throw exception");
		}
		assertTrue(cmaker.addRecipe(r));

		//assertEquals(cmaker.getRecipes().length, 1);
		
		/* Test adding same recipe - should fail */
		assertFalse(cmaker.addRecipe(r));
		
		/* Fill up the recipe book */
		for (int i = 0; i < 3; i++) {
			Recipe r2 = new Recipe();
			try {
				r2.setAmtChocolate("3");
				r2.setAmtCoffee("2");
				r2.setAmtMilk("3");
				r2.setAmtSugar("3");
				r2.setPrice("5");
				r2.setName("recipe" + Integer.toString(i));

			} catch (RecipeException e) {
				fail ("Shouldn't throw exception");
			}
			assertTrue(cmaker.addRecipe(r2));
			assertTrue(cmaker.getRecipes()[i+1].equals(r2));
		}
		Recipe r_fail = new Recipe();
		
		r_fail.setName("Shouldn'tAdd");				
		assertFalse(cmaker.addRecipe(r_fail));

		assertEquals(cmaker.getRecipes().length, 4);

	}

	@Test
	public void testEditRecipe() {
		CoffeeMaker cmaker = new CoffeeMaker();

		Recipe r = new Recipe();
		try {
			r.setAmtChocolate("1");
			r.setAmtCoffee("2");
			r.setAmtMilk("3");
			r.setAmtSugar("3");
			r.setPrice("5");
			r.setName("Test");
		} catch (RecipeException e) {
			fail ("Shouldn't throw exception");
		}
		assertTrue(cmaker.addRecipe(r));
		
		try {
			r.setAmtChocolate("3");
		} catch (RecipeException e) {
			fail ("Shouldn't throw exception");
		}
		
		assertEquals(cmaker.editRecipe(0, r), "Test");
		
		assertTrue(cmaker.getRecipes()[0].equals(r));
		assertEquals(cmaker.getRecipes()[0].getAmtChocolate(), 3);

		/* Test editing non-existing recipe */
		assertEquals(cmaker.editRecipe(1, r), null);

	}
	
	@Test
	public void testDeleteRecipe() {
		CoffeeMaker cmaker = new CoffeeMaker();

		Recipe r = new Recipe();
		try {
			r.setAmtChocolate("1");
			r.setAmtCoffee("2");
			r.setAmtMilk("3");
			r.setAmtSugar("3");
			r.setPrice("5");
			r.setName("Test");
		} catch (RecipeException e) {
			fail ("Shouldn't throw exception");
		}
		assertTrue(cmaker.addRecipe(r));
		
		assertEquals(cmaker.deleteRecipe(0), "Test");
		
		assertEquals(cmaker.getRecipes()[0].getName(), "");

		assertNull(cmaker.getRecipes()[1]);

	}
	
}
